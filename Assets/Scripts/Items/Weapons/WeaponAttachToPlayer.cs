﻿using System;
using Items.General;
using Player.Shooting;
using UnityEngine;

namespace Items.Weapons
{
	[RequireComponent(typeof(Rigidbody))]
	public class WeaponAttachToPlayer : MonoBehaviour
	{
		public WeaponInfo ThisWeaponInfo;
		public Transform WeaponBulletSpawnPoint;

		private GameObject player;
		private Rigidbody rb;
		private Transform thisTransform;
		private ItemSettings itemSettings;
		private PlayerShooting playerShooting;
		private PlayerTargeting playerTargeting;

		private void Awake()
		{
			player = GameObject.FindWithTag("Player");
			playerShooting = player.GetComponent<PlayerShooting>();
			playerTargeting = player.GetComponent<PlayerTargeting>();
			
			
			itemSettings = GetComponent<ItemSettings>();
			rb = GetComponent<Rigidbody>();
			
			thisTransform = transform;
		}

		public void AttachWeaponToPlayer(Transform boneToAttach)
		{
			rb.isKinematic = true;
			thisTransform.SetParent(boneToAttach);
			
			thisTransform.position = boneToAttach.position + ThisWeaponInfo.PositionAttachOffset;
			thisTransform.rotation = Quaternion.Euler(boneToAttach.rotation.eulerAngles + ThisWeaponInfo.RotationAttachOffset);
			
			itemSettings.ItemStatus = ItemStatus.AtPlayer;

			playerShooting.BulletSpawnPoint = WeaponBulletSpawnPoint;
		}

		public void DetachWeaponFromPlayer()
		{
			rb.isKinematic = false;
			thisTransform.SetParent(null);

			itemSettings.ItemStatus = ItemStatus.InWorld;
		}
	}
}
