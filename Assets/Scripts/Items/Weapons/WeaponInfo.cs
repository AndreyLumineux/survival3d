﻿using UnityEngine;

namespace Items.Weapons
{
    [CreateAssetMenu(fileName = "New Weapon Info", menuName = "Custom/WeaponInfo")]
    public class WeaponInfo : ScriptableObject
    {
        public Vector3 PositionAttachOffset;
        public Vector3 RotationAttachOffset;

        public ShootingType ShootingType;
        public float RawDamagePerHit;
        public float FireRate;
    }
}

public enum ShootingType
{
    Automatic,
    SemiAutomatic,
    Manual
}