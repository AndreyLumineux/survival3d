﻿using UnityEngine;

namespace Items.General
{
	public class ItemSettings : MonoBehaviour
	{
		public ItemStatus ItemStatus;
	}
}


public enum ItemStatus
{
	None,
	InWorld,
	AtPlayer
}