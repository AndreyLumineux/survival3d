﻿using System;
using Player.Interactions;
using UnityEngine;

namespace Items.General
{
	public class ItemMouseInteractions : MonoBehaviour
	{
		private GameObject player;
		private ItemSettings itemSettings;
		private Outline outline;
		private PlayerWorldItemsInteractions playerWorldItemsInteractions;

		private void Awake()
		{
			player = GameObject.FindWithTag("Player");

			playerWorldItemsInteractions = player.GetComponent<PlayerWorldItemsInteractions>();
			outline = GetComponent<Outline>();
			itemSettings = GetComponent<ItemSettings>();
		}

		private void OnMouseOver()
		{
			if (itemSettings.ItemStatus == ItemStatus.InWorld)
			{
				if (playerWorldItemsInteractions.SelectedWorldItem == null)
				{
					if (playerWorldItemsInteractions.SelectWorldItem(this.gameObject))
					{
						HighlightWorldItem();
					}
				}
			}
		}

		private void OnMouseExit()
		{
			if (itemSettings.ItemStatus == ItemStatus.InWorld)
			{
				if (playerWorldItemsInteractions.DeselectWorldItem(this.gameObject))
				{
					UnHighlightWorldItem();
				}
			}
		}

		private void HighlightWorldItem()
		{
			outline.ActivateOutline();
		}

		private void UnHighlightWorldItem()
		{
			outline.DeactivateOutline();
		}
	}
}