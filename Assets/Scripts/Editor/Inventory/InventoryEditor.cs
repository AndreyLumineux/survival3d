﻿using System.Globalization;
using Core.Inventory;
using UnityEditor;

namespace Editor.Inventory
{
  [CustomEditor(typeof(InventoryWeightedList))]
  public class InventoryEditor : DebugEditor<InventoryWeightedList>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField(nameof(Target.CurrentWeight), Target.CurrentWeight.ToString(CultureInfo.InvariantCulture));
      EditorGUILayout.LabelField(nameof(Target.MaxWeight), Target.MaxWeight.Value.ToString(CultureInfo.InvariantCulture));
    }
  }
}
