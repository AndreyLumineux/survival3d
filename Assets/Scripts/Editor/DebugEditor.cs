﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
  public abstract class DebugEditor<T> : UnityEditor.Editor
    where T : Object
  {
    private bool debugFoldout;

    protected T Target { get; private set; }

    private void Awake()
    {
      Target = (T) target;
    }

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      debugFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(debugFoldout, "Debug");
      if (debugFoldout)
        LayoutDebugInfo();
      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    public override bool RequiresConstantRepaint() => debugFoldout;

    protected abstract void LayoutDebugInfo();
  }
}
