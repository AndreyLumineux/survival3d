﻿using Core.Character.Health;
using UnityEditor;

namespace Editor.Character.Health
{
  [CustomEditor(typeof(HealthController))]
  public class HealthControllerEditor : DebugEditor<HealthController>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField("Health: ", $"{Target.CurrentHealth}/{Target.MaxHealth}");
    }
  }
}
