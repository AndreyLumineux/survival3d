﻿using Core.Character.Movement.Supplier.Abstract;
using UnityEditor;

namespace Editor.Character.Movement.Supplier
{
  [CustomEditor(typeof(AxisSupplierBase), true)]
  public class AxisSupplierEditor : DebugEditor<AxisSupplierBase>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField("", Target.GetVector().ToString());
    }
  }
}
