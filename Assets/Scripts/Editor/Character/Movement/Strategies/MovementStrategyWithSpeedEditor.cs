﻿using Core.Character.Movement.Strategies;
using Core.Character.Movement.Strategies.Abstract;
using UnityEditor;

namespace Editor.Character.Movement.Strategies
{
  [CustomEditor(typeof(MovementStrategyWithSpeed), true)]
  public class MovementStrategyWithSpeedEditor : DebugEditor<MovementStrategyWithSpeed>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField(nameof(IMovementStrategyWithSpeed.CurrentSpeed), Target.CurrentSpeed.ToString());
      EditorGUILayout.LabelField(nameof(IMovementStrategyWithSpeed.TargetSpeed), Target.TargetSpeed.ToString());
    }
  }
}
