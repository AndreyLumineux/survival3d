﻿using UnityEngine;

namespace Core.Character.Movement.Strategies.Abstract
{
  public interface IMovementStrategy
  {
    Vector3 GetPositionForNextFrame(Vector3 currentPosition);
  }
}
