﻿using UnityEngine;

namespace Core.Character.Movement.Strategies.Abstract
{
  public interface IMovementStrategyWithSpeed : IMovementStrategy
  {
    Vector3 CurrentSpeed { get; }

    Vector3 TargetSpeed { get; set; }
  }
}
