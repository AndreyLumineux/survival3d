﻿using Core.Character.Movement.Strategies.Abstract;
using Core.Character.Movement.Validation.Abstract;
using Extras;
using UnityEngine;

namespace Core.Character.Movement.Strategies
{
  [RequireComponent(typeof(IPositionValidator))]
  public class MovementStrategyWithSpeed : MonoBehaviour, IMovementStrategyWithSpeed
  {
    private IPositionValidator positionValidator;

    [SerializeField]
    private Damper speedDamper;

    public Vector3 CurrentSpeed => speedDamper.Current;

    public Vector3 TargetSpeed
    {
      get => speedDamper.Target;
      set => speedDamper.Target = value;
    }

    public Vector3 GetPositionForNextFrame(Vector3 currentPosition)
    {
      Vector3 target = currentPosition + CurrentSpeed * Time.deltaTime;
      return positionValidator.IsValidPosition(gameObject, target) ? target : positionValidator.GetClosestValidPosition(gameObject, target);
    }

    private void Awake()
    {
      positionValidator = GetComponent<IPositionValidator>();
    }

    private void Update()
    {
      speedDamper.Update();
    }
  }
}
