﻿using Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace Core.Character.Movement.Supplier
{
  public class AxisSupplierRawInputXZ : AxisSupplierBase
  {
    public override float GetX() => Input.GetAxisRaw("Horizontal");

    public override float GetY() => 0;

    public override float GetZ() => Input.GetAxisRaw("Vertical");
  }
}
