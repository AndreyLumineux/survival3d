﻿using Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace Core.Character.Movement.Supplier
{
  public class AxisSupplierRawInputXY : AxisSupplierBase
  {
    public override float GetX() => Input.GetAxisRaw("Horizontal");

    public override float GetY() => Input.GetAxisRaw("Vertical");

    public override float GetZ() => 0;
  }
}
