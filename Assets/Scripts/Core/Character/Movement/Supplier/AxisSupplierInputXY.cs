﻿using Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace Core.Character.Movement.Supplier
{
  public class AxisSupplierInputXY : AxisSupplierBase
  {
    public override float GetX() => Input.GetAxis("Horizontal");

    public override float GetY() => Input.GetAxis("Vertical");

    public override float GetZ() => 0;
  }
}
