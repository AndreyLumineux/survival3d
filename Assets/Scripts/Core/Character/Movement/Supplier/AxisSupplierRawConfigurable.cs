﻿using Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace Core.Character.Movement.Supplier
{
  public class AxisSupplierRawConfigurable : AxisSupplierBase
  {
    [SerializeField]
    private string xAxis;

    [SerializeField]
    private string yAxis;

    [SerializeField]
    private string zAxis;

    public override float GetX() => Input.GetAxisRaw(xAxis);

    public override float GetY() => Input.GetAxisRaw(yAxis);

    public override float GetZ() => Input.GetAxisRaw(zAxis);
  }
}
