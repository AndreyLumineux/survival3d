﻿using System;
using UnityEngine;

namespace Core.Character.Movement
{
  public class PositionController : MonoBehaviour
  {
    private Func<Vector3> getPositionDelegate;

    private new Rigidbody rigidbody;

    private new Rigidbody2D rigidbody2D;

    private Action<Vector3> setPositionDelegate;

    private void Awake()
    {
      rigidbody2D = GetComponent<Rigidbody2D>();
      rigidbody = GetComponent<Rigidbody>();
      if (rigidbody2D)
      {
        getPositionDelegate = () => rigidbody2D.position;
        setPositionDelegate = position => rigidbody2D.MovePosition(position);
      }
      else if (rigidbody)
      {
        getPositionDelegate = () => rigidbody.position;
        setPositionDelegate = position => rigidbody.MovePosition(position);
      }
      else
      {
        getPositionDelegate = () => transform.position;
        setPositionDelegate = position => transform.position = position;
      }
    }

    public Vector3 GetPosition() => getPositionDelegate.Invoke();

    public void SetPosition(Vector3 position) => setPositionDelegate.Invoke(position);
  }
}
