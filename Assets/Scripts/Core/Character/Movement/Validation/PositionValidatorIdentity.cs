﻿using Core.Character.Movement.Validation.Abstract;
using UnityEngine;

namespace Core.Character.Movement.Validation
{
  public class PositionValidatorIdentity : MonoBehaviour, IPositionValidator
  {
    public bool IsValidPosition(GameObject gameObject, Vector3 targetPosition) => true;

    public Vector3 GetClosestValidPosition(GameObject gameObject, Vector3 targetPosition) => targetPosition;
  }
}
