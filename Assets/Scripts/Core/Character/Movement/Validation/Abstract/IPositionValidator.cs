﻿using UnityEngine;

namespace Core.Character.Movement.Validation.Abstract
{
  public interface IPositionValidator
  {
    bool IsValidPosition(GameObject gameObject, Vector3 targetPosition);

    Vector3 GetClosestValidPosition(GameObject gameObject, Vector3 targetPosition);
  }
}
