﻿using Core.Character.Movement;
using Core.Character.Movement.Strategies.Abstract;
using Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace Core.Character
{
  [RequireComponent(typeof(IAxisSupplier), typeof(IMovementStrategyWithSpeed), typeof(PositionController))]
  public class CharacterController2 : MonoBehaviour
  {
    private IAxisSupplier axisSupplier;

    private IMovementStrategyWithSpeed movement;

    private PositionController positionController;

    private void Awake()
    {
      axisSupplier = GetComponent<IAxisSupplier>();
      movement = GetComponent<IMovementStrategyWithSpeed>();
      positionController = GetComponent<PositionController>();
    }

    private void Update()
    {
      movement.TargetSpeed = axisSupplier.GetVector();
      Vector3 position = movement.GetPositionForNextFrame(positionController.GetPosition());
      positionController.SetPosition(position);
    }
  }
}
