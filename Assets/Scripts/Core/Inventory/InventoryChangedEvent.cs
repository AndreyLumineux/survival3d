﻿using System;
using UnityEngine.Events;

namespace Core.Inventory
{
  [Serializable]
  public class InventoryChangedEvent : UnityEvent<Item> { }
}
