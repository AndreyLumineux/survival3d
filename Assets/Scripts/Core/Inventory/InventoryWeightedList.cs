﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extras;
using Extras.Properties;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Inventory
{
  public class InventoryWeightedList : MonoBehaviour, IInventory
  {
    private readonly IList<Item> items = new List<Item>();

    [SerializeField]
    private MultipliablePropertyFloat maxWeight = new MultipliablePropertyFloat();

    [SerializeField]
    private InventoryChangedEvent onItemAdded = new InventoryChangedEvent();

    [SerializeField]
    private InventoryChangedEvent onItemRemoved = new InventoryChangedEvent();

    public float CurrentWeight => items.Select(item => item.StackSize * item.ItemObject.Weight).Sum();

    public MultipliablePropertyFloat MaxWeight => maxWeight;

    public UnityEvent<Item> OnItemAdded => onItemAdded;

    public UnityEvent<Item> OnItemRemoved => onItemRemoved;

    public IEnumerable<Item> GetAllItems() => items;

    public bool ContainsItem(Item item)
    {
      Item existing = GetExistingItemOrNull(item);
      return existing != null && existing.StackSize >= item.StackSize;
    }

    public bool CanPutItem(Item item)
    {
      float additionalWeight = item.StackSize * item.ItemObject.Weight;
      return CurrentWeight + additionalWeight <= maxWeight;
    }

    public Item PutItem(Item item)
    {
      if (!CanPutItem(item))
        throw new InvalidOperationException();

      Item newItem;
      if (item.ItemObject.IsStackable)
      {
        ulong existingStackSize = (GetExistingItemOrNull(item)?.StackSize).GetValueOrDefault();
        ulong newStackSize = item.StackSize + existingStackSize;
        newItem = new Item(item.ItemObject, newStackSize);
      }
      else
        newItem = item;

      items.Add(newItem);
      OnItemAdded.Invoke(item);
      return newItem;
    }

    public void RemoveItem(Item item)
    {
      if (!ContainsItem(item))
        throw new InvalidOperationException();

      Item existingItem = GetExistingItemOrNull(item);
      items.Remove(existingItem);
      if (existingItem.StackSize > item.StackSize)
        items.Add(new Item(item.ItemObject, existingItem.StackSize - item.StackSize));
      OnItemRemoved?.Invoke(item);
    }

    private Item GetExistingItemOrNull(Item item) => items.FirstOrDefault(GetBoundItemEqualityPredicate(item));

    private static Func<Item, bool> GetBoundItemEqualityPredicate(Item item) => BoundComparer<Item>.Bind<ItemEqualityComparer>(item).EqualsOther;
  }
}
