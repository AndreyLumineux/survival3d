﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Core.Inventory
{
  public class ItemObjectEqualityComparer : IEqualityComparer<ItemObject>
  {
    public bool Equals(ItemObject x, ItemObject y)
    {
      bool xNull = !x;
      bool yNull = !y;
      bool bothNotNull = !xNull && !yNull;
      if (!bothNotNull)
        return xNull ^ yNull;

      Debug.Assert(x != null, nameof(x) + " != null");
      Debug.Assert(y != null, nameof(y) + " != null");
      return x.Id.Equals(y.Id);
    }

    public int GetHashCode(ItemObject obj) => obj.Id.GetHashCode();
  }
}
