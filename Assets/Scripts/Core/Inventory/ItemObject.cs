﻿using UnityEngine;

namespace Core.Inventory
{
  [CreateAssetMenu(menuName = "Items/New Item")]
  public abstract class ItemObject : ScriptableObject
  {
    [SerializeField]
    private long id;

    [SerializeField]
    private bool isStackable;

    [SerializeField]
    private string itemName;

    [SerializeField]
    private float weight;

    public long Id => id;

    public string ItemName => itemName;

    public bool IsStackable => isStackable;

    public float Weight => weight;
  }
}
