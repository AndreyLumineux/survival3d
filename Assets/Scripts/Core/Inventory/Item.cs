﻿using System;

namespace Core.Inventory
{
  public class Item
  {
    public Item(ItemObject itemObject, ulong stackSize)
    {
      if (stackSize == 0)
        throw new ArgumentOutOfRangeException(nameof(stackSize));
      if (itemObject.IsStackable == false && stackSize > 1)
        throw new ArgumentException(nameof(stackSize));

      ItemObject = itemObject;
      StackSize = stackSize;
    }

    public ulong StackSize { get; }

    public ItemObject ItemObject { get; }
  }
}
