﻿using System;
using UnityEngine;

namespace Extras
{
  [Serializable]
  public class Damper
  {
    private Vector3 currentVelocity = Vector3.zero;

    [SerializeField]
    [Range(0, 1)]
    private float smoothTime;

    public Vector3 Current { get; set; }
    public Vector3 Target { get; set; }
    public float SmoothTime => smoothTime;

    public Vector3 Update()
    {
      return Current = Vector3.SmoothDamp(Current, Target, ref currentVelocity, SmoothTime);
    }
  }
}
