﻿using System;
using Extras.Properties.Abstract;
using UnityEngine;

namespace Extras.Properties
{
  [Serializable]
  public class MultipliablePropertyFloat : PropertyBase<float>
  {
    [SerializeField]
    private MutablePropertyFloat additional = new MutablePropertyFloat();

    [SerializeField]
    private PropertyFloat baseValue = new PropertyFloat();

    [SerializeField]
    private MutablePropertyFloat multiplier = new MutablePropertyFloat(1);

    public IMutableProperty<float> Additional => additional;

    public IProperty<float> BaseValue => baseValue;

    public IMutableProperty<float> Multiplier => multiplier;

    public override float Value => BaseValue.Value * Multiplier.Value + Additional.Value;
  }
}
