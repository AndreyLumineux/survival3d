﻿namespace Extras.Properties
{
  public delegate void PropertyChangedEventHandler<in TProperty, in TValue>(TProperty property, TValue oldValue, TValue newValue);
}
