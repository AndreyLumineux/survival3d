﻿namespace Extras.Properties.Abstract
{
  public interface IProperty<out TValue>
  {
    TValue Value { get; }
  }
}
