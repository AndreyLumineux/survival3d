﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Extras.Properties.Abstract
{
  [Serializable]
  public abstract class MutableProperty<TValue> : IMutableProperty<TValue>
  {
    [SerializeField]
    private TValue value;

    protected MutableProperty() : this(default) { }

    protected MutableProperty(TValue value) => this.value = value;

    public virtual TValue Value
    {
      get => value;
      set
      {
        if (EqualityComparer<TValue>.Default.Equals(this.value, value))
          return;

        TValue old = this.value;
        this.value = value;
        Changed?.Invoke(this, old, value);
      }
    }

    public event PropertyChangedEventHandler<IMutableProperty<TValue>, TValue> Changed;

    public static implicit operator TValue(MutableProperty<TValue> property) => property.Value;
  }
}
