﻿namespace Extras.Properties.Abstract
{
  public interface IMutableProperty<TValue> : IProperty<TValue>
  {
    new TValue Value { get; set; }

    event PropertyChangedEventHandler<IMutableProperty<TValue>, TValue> Changed;
  }
}
