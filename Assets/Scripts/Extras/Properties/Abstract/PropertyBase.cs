﻿using System;

namespace Extras.Properties.Abstract
{
  [Serializable]
  public abstract class PropertyBase<TValue> : IProperty<TValue>
  {
    public abstract TValue Value { get; }

    public static implicit operator TValue(PropertyBase<TValue> property) => property.Value;
  }
}
