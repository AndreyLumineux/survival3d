﻿using System;

namespace Extras.Properties
{
  [Serializable]
  public class PropertyFloat : Property<float> { }
}
