﻿using System;
using Extras.Properties.Abstract;
using UnityEngine;

namespace Extras.Properties
{
  [Serializable]
  public class Property<TValue> : PropertyBase<TValue>
  {
    [SerializeField]
    private TValue value;

    protected Property() : this(default) { }

    protected Property(TValue value) => this.value = value;

    public override TValue Value => value;
  }
}
