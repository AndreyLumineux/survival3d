﻿using System;
using Extras.Properties.Abstract;

namespace Extras.Properties
{
  [Serializable]
  public class MutablePropertyFloat : MutableProperty<float>
  {
    public MutablePropertyFloat() : this(default) { }

    public MutablePropertyFloat(float value) : base(value) { }
  }
}
