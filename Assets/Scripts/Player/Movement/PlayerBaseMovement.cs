﻿using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Player.Movement
{
    public class PlayerBaseMovement : MonoBehaviour
    {
        public float PlayerSpeed;


        private Transform mainCameraTransform;
        private PlayerCollision playerCollision;
        private PlayerAnimationController playerAnimationController;


        private void Awake()
        {
            playerAnimationController = GetComponent<PlayerAnimationController>();
            
            Debug.Assert(Camera.main != null, "Camera.main != null");
            mainCameraTransform = Camera.main.transform;

            playerCollision = GetComponent<PlayerCollision>();
        }

        private void Update()
        {
            float verticalMovement = Input.GetAxis("Vertical");
            float horizontalMovement = Input.GetAxis("Horizontal");
            
            InputMovement(verticalMovement, horizontalMovement);
        }

        private void InputMovement(float verticalMovement, float horizontalMovement)
        {
            playerAnimationController.UpdateMovementAnimation(verticalMovement, horizontalMovement);
            
            Vector3 direction = Vector3.forward * verticalMovement + Vector3.right * horizontalMovement;
            if (playerCollision.CheckCollision(direction))
            {
                MovePlayer(direction);
            }
            
        }

        private void MovePlayer(Vector3 direction)
        {
            gameObject.transform.Translate(PlayerSpeed * Time.deltaTime * direction, Space.World);
        }
    }
}
