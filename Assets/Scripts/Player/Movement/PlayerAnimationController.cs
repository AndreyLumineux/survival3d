﻿using Player.Shooting;
using UnityEngine;

namespace Player.Movement
{
	public class PlayerAnimationController : MonoBehaviour
	{
		private Animator animator;
		private PlayerTargeting playerTargeting;
		
		private bool alreadyMoving;

		private static readonly int IsMovingAnimationHash = Animator.StringToHash("isMoving");
		private static readonly int MovingRightAnimationHash = Animator.StringToHash("MovingRightFloat");
		private static readonly int MovingForwardAnimationHash = Animator.StringToHash("MovingForwardFloat");
		private static readonly int IsAimingAnimationHash = Animator.StringToHash("isAiming");
		
		private void Awake()
		{
			playerTargeting = GetComponent<PlayerTargeting>();

			animator = GetComponentInChildren<Animator>();
		}

		public void UpdateMovementAnimation(float verticalMovement, float horizontalMovement)
		{
			if (!(Mathf.Approximately(verticalMovement, 0.0f) && Mathf.Approximately(horizontalMovement, 0.0f)))
			{
				Vector3 targetVector = (playerTargeting.PlayerTargetingPosition - transform.position).normalized;
				Vector2 targetDirection = new Vector2(targetVector.x, targetVector.z);
				Vector2 movementVector = new Vector2(horizontalMovement, verticalMovement);
				float angle = Vector2.SignedAngle(targetDirection, Vector2.up);

				Vector2 resultVector = Quaternion.AngleAxis(angle, Vector3.forward) * movementVector;
				resultVector.Normalize();

				if (alreadyMoving)
				{
					animator.SetFloat(MovingRightAnimationHash, resultVector.x);
					animator.SetFloat(MovingForwardAnimationHash, resultVector.y);
				}
				else
				{
					animator.SetBool(IsMovingAnimationHash, true);
					animator.SetFloat(MovingRightAnimationHash, resultVector.x);
					animator.SetFloat(MovingForwardAnimationHash, resultVector.y);

					alreadyMoving = true;
				}
			}
			else
			{
				if (alreadyMoving)
				{
					StopMovementAnimation();
				}
			}
		}

		private void StopMovementAnimation()
		{
			animator.SetBool(IsMovingAnimationHash, false);
			alreadyMoving = false;
		}

		public void StartAimingAnimation()
		{
			animator.SetBool(IsAimingAnimationHash, true);
		}
		
		public void StopAimingAnimation()
		{
			animator.SetBool(IsAimingAnimationHash, false);
		}
	}
}