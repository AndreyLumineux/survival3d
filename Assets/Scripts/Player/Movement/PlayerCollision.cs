﻿using UnityEngine;

namespace Player.Movement
{
    public class PlayerCollision : MonoBehaviour
    {
        public bool CheckCollision(Vector3 direction)
        {
            LayerMask layerMask = LayerMask.GetMask("Default");
            
            Transform playerTransform = transform;
            Vector3 raycastOrigin = playerTransform.position + Vector3.up * 0.5f;
            if (Physics.SphereCast(raycastOrigin, 0.25f, direction, out RaycastHit hitInfo, 0.25f, layerMask))
            {
                return false;
            }
            
            return true;
        }
    }
}
