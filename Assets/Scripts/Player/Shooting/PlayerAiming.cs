﻿using Items.Weapons;
using Player.Movement;
using UnityEngine;

namespace Player.Shooting
{
	public class PlayerAiming : MonoBehaviour
	{
		public bool IsPlayerAiming;

		public bool CanPlayerAim = true;

		private PlayerAnimationController playerAnimationController;

		private PlayerHolding playerHolding;

		private void Awake()
		{
			playerHolding = GetComponent<PlayerHolding>();
			playerAnimationController = GetComponent<PlayerAnimationController>();
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown(1))
			{
				PlayerStartAiming();
			}

			if (Input.GetMouseButtonUp(1))
			{
				PlayerStopAiming();
			}
		}

		private void PlayerStartAiming()
		{
			if (!CanPlayerAim)
				return;

			playerAnimationController.StartAimingAnimation();
			IsPlayerAiming = true;
		}

		public void PlayerStopAiming()
		{
			playerAnimationController.StopAimingAnimation();
			IsPlayerAiming = false;
			playerHolding.CurrentHoldingItem.transform.localRotation = Quaternion.Euler(playerHolding.CurrentHoldingItem
				.GetComponent<WeaponAttachToPlayer>().ThisWeaponInfo.RotationAttachOffset);
		}
	}
}
