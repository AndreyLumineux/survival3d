﻿using System;
using UnityEngine;

namespace Player.Shooting
{
	public class PlayerShooting : MonoBehaviour
	{
		public GameObject BulletPrefab;

		public Transform BulletSpawnPoint;

		private PlayerAiming playerAiming;

		private PlayerHolding playerHolding;

		private float lastShotTime;

		private void Awake()
		{
			playerHolding = GetComponent<PlayerHolding>();
			playerAiming = GetComponent<PlayerAiming>();
		}

		private void Update()
		{
			ShootingType shootingType = playerHolding.WeaponAttachToPlayer.ThisWeaponInfo.ShootingType;
			switch (shootingType)
			{
				case ShootingType.SemiAutomatic: case ShootingType.Manual:
				{
					if (Input.GetMouseButtonDown(0))
					{
						PlayerShoot();
					}

					break;
				}
				case ShootingType.Automatic:
				{
					if (Input.GetMouseButton(0))
					{
						PlayerShoot();
					}

					break;
				}
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void PlayerShoot()
		{
			if (!playerAiming.IsPlayerAiming)
				return;

			if (lastShotTime + 1 / playerHolding.WeaponAttachToPlayer.ThisWeaponInfo.FireRate <= Time.time)
			{
				lastShotTime = Time.time;
				GameObject bullet = Instantiate(BulletPrefab, BulletSpawnPoint.position,
					Quaternion.Euler(BulletSpawnPoint.rotation.eulerAngles +
					                 Quaternion.AngleAxis(180f, Vector3.up).eulerAngles));
				Destroy(bullet, 1f);
			}
		}

		// private Vector3 FindBulletTargetingPosition(float bulletYPosition)
		// {
		// 	LayerMask groundLayerMask = LayerMask.GetMask("Ground");
		// 	Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
		// 	if (Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, groundLayerMask))
		// 	{
		// 		Vector3 c = hit.point;
		// 		Vector3 b = hit.point;
		// 		b.y = bulletYPosition;
		// 		Vector3 cb = b - c;
		// 		Vector3 ca = Vector3.Project(cb, ray.direction);
		// 		Vector3 a = hit.point + ca;
		// 		Vector3 r = ca;
		// 		float alpha = bulletYPosition / a.y;
		// 		r *= alpha;
		// 		return hit.point + r;
		// 	}
		//
		// 	return Vector3.zero;
		// }
	}
}
