﻿using System;
using System.Collections.Generic;
using Items.Weapons;
using UnityEngine;

namespace Player.Shooting
{
	public class PlayerHolding : MonoBehaviour
	{
		public GameObject CurrentHoldingItem;
		public Transform BoneToAttach;
		//TODO: Holster item
		public List<GameObject> TestObjects;
		public WeaponAttachToPlayer WeaponAttachToPlayer;

		private int testWeaponIndex;

		private void Start()
		{
			if (TestObjects.Count > 0)
			{
				PlayerStartHoldingItem(TestObjects[0]);
			}
		}

		private void PlayerStartHoldingItem(GameObject objectToHold)
		{
			if(CurrentHoldingItem != null)
				return;

			CurrentHoldingItem = objectToHold;

			WeaponAttachToPlayer = objectToHold.GetComponent<WeaponAttachToPlayer>();
			if (WeaponAttachToPlayer != null)
			{
				WeaponAttachToPlayer.AttachWeaponToPlayer(BoneToAttach);
			}
		}

		private void PlayerRemoveHoldingWeapon()
		{
			if(CurrentHoldingItem == null)
				return;

			WeaponAttachToPlayer.DetachWeaponFromPlayer();

			WeaponAttachToPlayer = null;
			CurrentHoldingItem = null;
		}
	}
}
