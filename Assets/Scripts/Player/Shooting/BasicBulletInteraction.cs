﻿using UnityEngine;

namespace Player.Shooting
{
    public class BasicBulletInteraction : MonoBehaviour
    {
        public bool ActiveBullet = true;
        public float BulletSpeed = 5f;

        private void Update()
        {
            if (ActiveBullet)
            {
                MoveBullet(BulletSpeed);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!(other.CompareTag("Bullet") || other.CompareTag("Player") || other.CompareTag("Item")))
            {
                ActiveBullet = false;                
            }
        }

        private void MoveBullet(float bulletSpeed)
        {
            transform.Translate(bulletSpeed * Time.deltaTime * Vector3.forward);
        }
    }
}
