﻿using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Player.Shooting
{
	public class PlayerTargeting : MonoBehaviour
	{
		public bool InstantRotation = true;
		public float RotationSpeed = 7f;
		public GameObject Ball;

		[HideInInspector] public Vector3 PlayerTargetingPosition;

		private PlayerAiming playerAiming;
		private PlayerHolding playerHolding;
		private Camera mainCamera;

		private void Awake()
		{
			playerHolding = GetComponent<PlayerHolding>();
			playerAiming = GetComponent<PlayerAiming>();

			Debug.Assert(Camera.main != null, "Camera.main != null");
			mainCamera = Camera.main;
		}

		private void Update()
		{
			Vector3 targetPoint = FindPlayerTargetingPosition(transform.position.y);
			PlayerTargetingPosition = targetPoint;
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Instantiate(Ball, targetPoint, Quaternion.identity);
			}

			if (Vector3.Distance(transform.position, playerHolding.CurrentHoldingItem.transform.position) + 0.5f >=
			    Vector3.Distance(transform.position, targetPoint))
			{
				if (playerAiming.IsPlayerAiming)
				{
					playerAiming.PlayerStopAiming();
				}

				playerAiming.CanPlayerAim = false;
			}
			else
			{
				playerAiming.CanPlayerAim = true;
			}

			
			if (playerAiming.IsPlayerAiming)
			{
				playerHolding.CurrentHoldingItem.transform.LookAt(targetPoint);
			}

			RotatePlayerTowardsPoint(targetPoint);
		}

		private Vector3 FindPlayerTargetingPosition(float playerYPosition)
		{
			LayerMask groundLayerMask = LayerMask.GetMask("Ground");

			Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, groundLayerMask))
			{
				Vector3 c = hit.point;
				Vector3 b = hit.point;
				b.y = playerYPosition;
				Vector3 cb = b - c;
				Vector3 ca = Vector3.Project(cb, ray.direction);
				Vector3 a = hit.point + ca;
				Vector3 r = ca;
				float alpha = playerYPosition / a.y;
				r *= alpha;
				return hit.point + r;
			}

			return Vector3.zero;
		}

		private void RotatePlayerTowardsPoint(Vector3 targetPoint)
		{
			Vector3 targetRotationEuler = Quaternion.LookRotation(targetPoint - transform.position).eulerAngles;
			Quaternion targetRotation = Quaternion.Euler(new Vector3(0, targetRotationEuler.y, 0));

			transform.rotation = InstantRotation
				? targetRotation
				: Quaternion.Slerp(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
		}
	}
}