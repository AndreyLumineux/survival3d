﻿using UnityEditorInternal;
using UnityEngine;

namespace Player.Interactions
{
    public class PlayerWorldItemsInteractions : MonoBehaviour
    {
        public GameObject SelectedWorldItem;

        public bool SelectWorldItem(GameObject item)
        {
            if(item == null)
                return false;
            
            SelectedWorldItem = item;
            return true;
        }

        public bool DeselectWorldItem(GameObject item)
        {
            if (SelectedWorldItem != item) // Check if the item we want to deselect is the actual selected item
                return false;
            
            SelectedWorldItem = null;
            return true;
        }

        public void PickUpSelectedWorldItem()
        {
            PickUpItem(SelectedWorldItem);
        }

        private void PickUpItem(GameObject item)
        {
            
        }
    }
}
